﻿namespace PeerToPeer
{
    partial class FrmUdpPeer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Send = new System.Windows.Forms.Button();
            this.btn_Connention = new System.Windows.Forms.Button();
            this.rtb_Msg = new System.Windows.Forms.RichTextBox();
            this.txt_Msg = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_Send
            // 
            this.btn_Send.Enabled = false;
            this.btn_Send.Location = new System.Drawing.Point(648, 519);
            this.btn_Send.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Send.Name = "btn_Send";
            this.btn_Send.Size = new System.Drawing.Size(100, 29);
            this.btn_Send.TabIndex = 4;
            this.btn_Send.Text = "发送";
            this.btn_Send.UseVisualStyleBackColor = true;
            this.btn_Send.Click += new System.EventHandler(this.btn_Send_Click);
            // 
            // btn_Connention
            // 
            this.btn_Connention.Location = new System.Drawing.Point(72, 519);
            this.btn_Connention.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Connention.Name = "btn_Connention";
            this.btn_Connention.Size = new System.Drawing.Size(100, 29);
            this.btn_Connention.TabIndex = 3;
            this.btn_Connention.Text = "链接";
            this.btn_Connention.UseVisualStyleBackColor = true;
            this.btn_Connention.Click += new System.EventHandler(this.btn_Connention_Click);
            // 
            // rtb_Msg
            // 
            this.rtb_Msg.Location = new System.Drawing.Point(17, 15);
            this.rtb_Msg.Margin = new System.Windows.Forms.Padding(4);
            this.rtb_Msg.Name = "rtb_Msg";
            this.rtb_Msg.Size = new System.Drawing.Size(1032, 489);
            this.rtb_Msg.TabIndex = 2;
            this.rtb_Msg.Text = "";
            // 
            // txt_Msg
            // 
            this.txt_Msg.Location = new System.Drawing.Point(259, 523);
            this.txt_Msg.Name = "txt_Msg";
            this.txt_Msg.Size = new System.Drawing.Size(370, 25);
            this.txt_Msg.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(193, 533);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "发送框:";
            // 
            // FrmUdpPeer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txt_Msg);
            this.Controls.Add(this.btn_Send);
            this.Controls.Add(this.btn_Connention);
            this.Controls.Add(this.rtb_Msg);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmUdpPeer";
            this.Text = "FrmUdpPeer";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Send;
        private System.Windows.Forms.Button btn_Connention;
        private System.Windows.Forms.RichTextBox rtb_Msg;
        private System.Windows.Forms.TextBox txt_Msg;
        private System.Windows.Forms.Label label1;
    }
}