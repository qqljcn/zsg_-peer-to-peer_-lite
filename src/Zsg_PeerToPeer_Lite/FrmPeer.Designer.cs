﻿namespace PeerToPeer
{
    partial class FrmPeer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rtb_Msg = new System.Windows.Forms.RichTextBox();
            this.btn_Connention = new System.Windows.Forms.Button();
            this.btn_Send = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rtb_Msg
            // 
            this.rtb_Msg.Location = new System.Drawing.Point(12, 12);
            this.rtb_Msg.Name = "rtb_Msg";
            this.rtb_Msg.Size = new System.Drawing.Size(775, 392);
            this.rtb_Msg.TabIndex = 0;
            this.rtb_Msg.Text = "";
            // 
            // btn_Connention
            // 
            this.btn_Connention.Location = new System.Drawing.Point(53, 415);
            this.btn_Connention.Name = "btn_Connention";
            this.btn_Connention.Size = new System.Drawing.Size(75, 23);
            this.btn_Connention.TabIndex = 1;
            this.btn_Connention.Text = "链接";
            this.btn_Connention.UseVisualStyleBackColor = true;
            this.btn_Connention.Click += new System.EventHandler(this.btn_Connention_Click);
            // 
            // btn_Send
            // 
            this.btn_Send.Enabled = false;
            this.btn_Send.Location = new System.Drawing.Point(485, 415);
            this.btn_Send.Name = "btn_Send";
            this.btn_Send.Size = new System.Drawing.Size(75, 23);
            this.btn_Send.TabIndex = 1;
            this.btn_Send.Text = "发送";
            this.btn_Send.UseVisualStyleBackColor = true;
            this.btn_Send.Click += new System.EventHandler(this.btn_Send_Click);
            // 
            // FrmPeer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_Send);
            this.Controls.Add(this.btn_Connention);
            this.Controls.Add(this.rtb_Msg);
            this.Name = "FrmPeer";
            this.Text = "FrmPeer";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rtb_Msg;
        private System.Windows.Forms.Button btn_Connention;
        private System.Windows.Forms.Button btn_Send;
    }
}